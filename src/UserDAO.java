

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import static common.JDBCTemplate.close;

public class UserDAO {

	private Properties prop = new Properties();
	
	public UserDAO() {
		try {
			prop.loadFromXML(new FileInputStream("mapper/user-query.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public int selectUserNo(Connection con, Map<String, String> userInfo) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		int result = 0;
		
		String query = prop.getProperty("selectUserNo");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userInfo.get("id"));
			pstmt.setString(2, userInfo.get("pwd"));
			
			rset = pstmt.executeQuery();
			if(rset.next()) {
				result = rset.getInt("USER_NO");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return result;
	}

	// 로그인 성공시
	// 회원번호 리턴
	
}
