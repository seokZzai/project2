
import java.awt.Color;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class NewLoginFrame extends JFrame {

	public NewLoginFrame() {

		this.setSize(1200, 800);
		
		JPanel firstPanel = new JPanel();
		firstPanel.setLayout(null);
		firstPanel.setBounds(0, 0, 1200, 800);
		firstPanel.setBackground(Color.WHITE);
				
		JLabel titleLabel1 = new JLabel("★우당탕탕★");
		titleLabel1.setBounds(800, 50, 250, 80);
		titleLabel1.setFont(titleLabel1.getFont().deriveFont(40.0f));
		
		JLabel titleLabel2 = new JLabel("☆좌충우돌☆");
		titleLabel2.setBounds(800, 130, 250, 80);
		titleLabel2.setFont(titleLabel2.getFont().deriveFont(40.0f));
		
		JLabel titleLabel3 = new JLabel("★무인도★");
		titleLabel3.setBounds(800, 210, 250, 80);
		titleLabel3.setFont(titleLabel3.getFont().deriveFont(40.0f));
		
		JLabel titleLabel4 = new JLabel("☆생존일기☆");
		titleLabel4.setBounds(800, 290, 250, 80);
		titleLabel4.setFont(titleLabel4.getFont().deriveFont(40.0f));
		
		Image icon1 = new ImageIcon("image/islandandmen.png").getImage().getScaledInstance(720, 800, 0);
		JLabel iconLabel = new JLabel(new ImageIcon(icon1));
		iconLabel.setBounds(0, 0, 720, 800);
			
		JTextField id = new JTextField(20);
		id.setBounds(900, 480, 250, 70);	
		
		JLabel idLabel = new JLabel("아이디");
		idLabel.setBounds(800, 480, 250, 70);
		idLabel.setFont(idLabel.getFont().deriveFont(30.0f));		
		
		JPasswordField password = new JPasswordField(30);
		password.setBounds(900, 580, 250, 70);		
		
		JLabel pwdLabel = new JLabel("비밀번호");
		pwdLabel.setBounds(769, 580, 250, 70);
		pwdLabel.setFont(pwdLabel.getFont().deriveFont(30.0f));		
		
		JLabel loginLabel = new JLabel("로그인");
		loginLabel.setBounds(850, 640, 170, 70);
		loginLabel.setFont(loginLabel.getFont().deriveFont(40.0f));
		
		JLabel searchLabel = new JLabel("ID/PW찾기");
		searchLabel.setBounds(950, 690, 200, 70);
		searchLabel.setFont(searchLabel.getFont().deriveFont(33.0f));
		
		JLabel createLabel = new JLabel("회원가입");
		createLabel.setBounds(770, 690, 170, 70);
		createLabel.setFont(createLabel.getFont().deriveFont(40.0f));
						
		firstPanel.add(iconLabel);
		firstPanel.add(titleLabel1);
		firstPanel.add(titleLabel2);
		firstPanel.add(titleLabel3);		
		firstPanel.add(titleLabel4);	
		firstPanel.add(id);
		firstPanel.add(idLabel);
		firstPanel.add(password);
		firstPanel.add(pwdLabel);
		firstPanel.add(loginLabel);
		firstPanel.add(searchLabel);
		firstPanel.add(createLabel);
		
		JPanel secondPanel = new JPanel();
		secondPanel.setLayout(null);
		secondPanel.setBounds(0, 0, 1200, 800);
		secondPanel.setBackground(Color.WHITE);
		
		JTextField inputNewId = new JTextField(20);
		inputNewId.setBounds(900, 450, 250, 60);
		
		JLabel newIdLabel = new JLabel("아이디");
		newIdLabel.setBounds(800, 450, 250, 60);
		newIdLabel.setFont(newIdLabel.getFont().deriveFont(30.0f));		
		
		JPasswordField inputNewPwd = new JPasswordField(20);
		inputNewPwd.setBounds(900, 520, 250, 60);
		
		JLabel newPwdLabel = new JLabel("비밀번호");
		newPwdLabel.setBounds(769, 520, 250, 60);	
		newPwdLabel.setFont(newPwdLabel.getFont().deriveFont(30.0f));	
		
		JLabel Email = new JLabel("이메일");
		Email.setBounds(800, 590, 250, 60);
		Email.setFont(Email.getFont().deriveFont(30.0f));
		
		JTextField inputNewEmail = new JTextField(30);
		inputNewEmail.setBounds(900, 590, 250, 60);
		
		JLabel regist = new JLabel("회원가입");
		regist.setBounds(770, 690, 170, 70);
		regist.setFont(regist.getFont().deriveFont(40.0f));
		
		JLabel cancelLabel = new JLabel("취소");
		cancelLabel.setBounds(1000, 690, 200, 70);
		cancelLabel.setFont(cancelLabel.getFont().deriveFont(40.0f));
		
		secondPanel.add(iconLabel);
		secondPanel.add(titleLabel1);
		secondPanel.add(titleLabel2);
		secondPanel.add(titleLabel3);
		secondPanel.add(titleLabel4);
		secondPanel.add(inputNewId);
		secondPanel.add(newIdLabel);
		secondPanel.add(inputNewPwd);
		secondPanel.add(newPwdLabel);
		secondPanel.add(Email);
		secondPanel.add(inputNewEmail);
		secondPanel.add(regist);
		secondPanel.add(cancelLabel);
		 
		JPanel thirdPanel = new JPanel();
		thirdPanel.setLayout(null);
		thirdPanel.setBounds(0, 0, 1200, 800);
		thirdPanel.setBackground(Color.WHITE);
		
		JLabel userId = new JLabel(id.getText());
		userId.setBounds(900, 400, 230, 70);
		userId.setFont(userId.getFont().deriveFont(30,0f));
		
		JLabel newGame = new JLabel("새 게임");
		newGame.setBounds(920, 490, 200, 70);
		newGame.setFont(newGame.getFont().deriveFont(30,0f));
		
		JLabel loadGame = new JLabel("불러오기");
		loadGame.setBounds(895, 580, 235, 70);
		loadGame.setFont(loadGame.getFont().deriveFont(30,0f));
		
		thirdPanel.add(iconLabel);
		thirdPanel.add(titleLabel1);
		thirdPanel.add(titleLabel2);
		thirdPanel.add(titleLabel3);
		thirdPanel.add(titleLabel4);
		thirdPanel.add(userId);
		thirdPanel.add(newGame);
		thirdPanel.add(loadGame);	
		
		
		
		this.add(firstPanel);
//		this.add(secondPanel);
//		this.add(thirdPanel);
				
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
// 1번패널 로그인화면
// 2번패널 회원가입화면
// 3번패널 게임선택화면