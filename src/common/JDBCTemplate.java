package common;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JDBCTemplate {

	public static Connection getConnection() {
		
		Connection con = null;
		
		Properties prop = new Properties();
		
		try {
			prop.load(new FileReader("config/connection-info.properties"));
			String driver = prop.getProperty("driver");
			String url = prop.getProperty("url");
			
			Class.forName(driver);
			
			con = DriverManager.getConnection(url, prop);
		
			con.setAutoCommit(false);
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return con;
	}
	
	public static void close(Connection con) {
		try {
			if(con != null && !con.isClosed()) {// 앞,뒤가 바뀌면 !con.isClosed부터 처리하기때문에 뒤에 != null 은  무시 당할 수 가있다
				con.close();					// connection이 닫히지 않았을때 넣어줌 ( !con.isClosed()에 느낌표 없을때가  닫히지 않았을때  )
			}									
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void close(Statement stmt) {
		try {
			if(stmt != null && !stmt.isClosed()) {
				stmt.close();					// null값에서 닫으려하면 널포인트익셉션 떠서 오작동 뜰 수 있어서 뒤에 && !~ 붙여줌 // 널포인트익셉션 방지
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}	
		
		public static void close(ResultSet rset) {
			try {
				if(rset != null && !rset.isClosed()) {
					rset.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
		
	public static void commit(Connection con) {
		try {
			if(con != null && !con.isClosed()) {		// && ! << 느낌표 빠져있으면 커밋을 만날 수 없다. if문으로 인식하지않기에?
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		
	public static void rollback(Connection con) {
		try {
			if(con != null && !con.isClosed()) {
				con.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
	



//-----------------------------------------------------------------------

// String str = "java";
//
// if("java".equals(str)) {	// java 자체를 str과 비교 한다.
// 	System.out.println("자바다");
// --------------------------------

// String str = "java";
//
// if(str.equals("java")) {	// str 자체를 java와 비교한다.
//	System.out.println("자바다");
// --------------------------------

// String str = "java";
//
// if(str != null && str.equals("java")) {	// null값을 방지하기 위한 것 
//	System.out.println("자바다");

// 위를 더 많이 사용한다. (널포인트익셉션 방지)		>> N P C 라고 줄여서 부른다.
// 두번째처럼하면 str = null; 이면 참조연산이 null이니까 비교 자체가 안되어서 널포인트익셉션이 뜬다.

//--------------------------------------------------------------------------
// 앞으로 문자열 비교할 때 첫번째처럼 하기!!



















