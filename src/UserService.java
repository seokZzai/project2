import java.sql.Connection;
import java.util.Map;
import static common.JDBCTemplate.getConnection;
import static common.JDBCTemplate.commit;
import static common.JDBCTemplate.rollback;


public class UserService {

	public int selectUserNo(Map<String, String> userInfo) {
		
		Connection con = getConnection();
		
		UserDAO udao = new UserDAO();
		
		int result = udao.selectUserNo(con, userInfo);
		
		if (result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

}
